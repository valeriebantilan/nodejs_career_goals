import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const BooksSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    user: { type: Schema.ObjectId, ref: "Users", required: true },
},
{
    timestamps: true
}
);

export default mongoose.model('Books', BooksSchema);