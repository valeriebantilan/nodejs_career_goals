import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import { comparePassword } from '../../utils/password';

const Schema = mongoose.Schema;

const UsersSchema = new Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
},
{
    timestamps: true
}
);

UsersSchema.pre('save', async function (next) {
    const saltRounds = 10;
    let user = this;

    // if (!user.isModified('password')) return next();

    try {

        user.password = await bcrypt.hash(user.password, saltRounds);
        next();
    } catch (error) {
        console.log(error);
        return next(error);
    }
}); 

UsersSchema.methods.comparePassword = function (candidatePassword, cb) {
    return comparePassword(candidatePassword, this.password, cb);
}; 

export default mongoose.model('Users', UsersSchema);