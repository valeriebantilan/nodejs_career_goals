const { Joi } = require('express-validation')

module.exports = {
  // POST /api/books
  create: {
    body:  Joi.object({
      title: Joi.string().required(),
      description: Joi.string().required(),
    }),
  },
  update: { 
    body: Joi.object({
      title: Joi.string(),
      description: Joi.string(),
    }),
  },
};
