import express from 'express';
import BooksController from '../controllers/BooksController'
import { validate } from 'express-validation';
import paramValidation from '../validation/books';
import { isAuthenticated } from '../../middleware/auth';

const routes = express.Router();

routes
    .get('/detail/:bookId',
    BooksController.queries.getBookById);
routes
    .get('/all',
    BooksController.queries.getBooks);

routes
    .post('/create',
      validate(paramValidation.create),
        isAuthenticated,
        BooksController.mutation.createBooks);
routes
    .post('/update/:bookId',
      validate(paramValidation.update),
        isAuthenticated,
        BooksController.mutation.updateBooks);
      
export default routes;