import express from 'express';
const { validate } = require('express-validation');
import AuthController from '../controllers/AuthController'
import paramValidation from '../validation/auth';

const routes = express.Router();

routes.post('/user/login',
  validate(paramValidation.login),
  AuthController.mutation.login
)

export default routes;