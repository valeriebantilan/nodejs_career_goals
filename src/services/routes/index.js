import express from 'express';
import momentTimezone from 'moment-timezone';
import users from './users';
import auth from './auth';
import books from './books';

const routes = express.Router();

routes.get('/', async (req, res) => {
    res.status(200).json({ message: 'Connected!' });
    console.log(momentTimezone.tz.guess());
});

routes.get('/health-check', async (req, res) => {
    res.status(200).json({ message: 'Health Check!' });
});

// routes.use('/api/', auth);

routes.use('/api/user', users);
routes.use('/api/books', books);

routes.use('/api', auth);


export default routes;