import express from 'express';
import UserController from '../controllers/UserController'
import { validate } from 'express-validation';
import paramValidation from '../validation/users';
import { isAuthenticated } from '../../middleware/auth';

const routes = express.Router();

routes
  .post('/create',
      validate(paramValidation.create),
      UserController.mutations.createUser);

routes
    .post('/update/:id',
        validate(paramValidation.update),
        isAuthenticated,
        UserController.mutations.updateUser);

routes
    .get('/single/:id',
      UserController.queries.getUserById);
routes
    .get('/all',
      UserController.queries.getUsers);
        
export default routes;