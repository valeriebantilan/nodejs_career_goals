import UsersSchema from '../models/Users';
import { createTokenWithExpiry, createRefreshToken } from '../../utils/auth';

const AuthController = {
  mutation: {
    login: async (req, res, next) => {
      try {
        const { email, password } = req.body;

        const findUser = await UsersSchema.findOne({ email }).exec();
        
        if (!findUser) return next(new Error('User does not exists.'));
  
        findUser.comparePassword(password, (err, user) => {
            if (err) return next(new Error('Something went wrong'));
  
          if (!user) {
            console.log('here');
            return next(new Error('Password mismatch'));
          } else {
            const expiresIn = '1D';
  
            const accessToken = createTokenWithExpiry(
              {
                _id: findUser.id,
              }, expiresIn);
            const refreshToken = createRefreshToken(
              {
                _id: findUser.id,
              });
      
            const token = {
                success: true,
                accessToken,
                refreshToken,
                expiresIn,
                refreshExpiresIn: '7D',
                user_id: findUser.id,
            };
    
            return res.status(200).json(token);
          }
        });
       
      } catch (error) {
        console.log(error);
        return next(error);
      }
    }
  }
}


export default AuthController;
