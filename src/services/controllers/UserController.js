// async await handlers is express.js
require('express-async-errors');

import UsersSchema from '../models/Users';


const UsersController = {
    queries: {
        getUserById: async (req, res, next) => {
            const { id: _id } = req.params;
            try {

                const user = await UsersSchema.findById({ _id }).exec();

                if (!user) return next(new Error('User not found'));

                return res.status(200).send({success: true, user});
            } catch (error) {
                return res.json(error);
            }
        },
        getUsers: async (req, res, next) => {
            try {
                const user =  await UsersSchema.find().exec();

                if (!user) return next(new Error('Empty data'));

                return res.status(200).send({success: true, user});
                
            } catch (error) {
                return res.json(error);
            }
        }
    },
    mutations: {
        createUser: async (req, res, next) => {

            const newUsers = new UsersSchema(req.body);
        
            try {
        
                //check email and password
        
                const findUser = await UsersSchema.findOne({ email: req.body.email }).exec();
        
                if (findUser) {
        
                    let error = new Error('Email address already exists');
        
                    return next(error);
                }
        
                const user = await newUsers.save();
        
                if (!user) throw new Error('Something went wrong');
        
                return res.status(200).send({success: true, user});
            } catch (error) {
                console.log(error);
                return next(error);
            }
        },
        updateUser: async (req, res) => {
            try {
                const { id } = req.params;
                const data = req.body;
        
                const updateUserById = await UsersSchema.findByIdAndUpdate(
                    { _id: id }, { ...data }, { new: true }).exec();
        
                if (!updateUserById) throw new Error('Something went wrong in updating');
                
                return res.status(200).send({ success: true, updateUserById });
            } catch (error) {
                console.log(error);
                return res.status(500).send(error);
            }
        }

    }
}

export default UsersController;