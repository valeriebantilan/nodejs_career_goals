import BooksSchema from '../models/Books';

const BooksController = {
  queries: {
    getBooks: async(req, res, next) => {
        try {
            const books =  await BooksSchema.find().exec();

            if (!books) return next(new Error('Empty data'));

            return res.status(200).send({success: true, books});
            
        } catch (error) {
            return res.json(error);
        }
    },
    getBookById: async(req, res, next) => {
        const { bookId: _id } = req.params;
        try {

            const book = await BooksSchema.findById({ _id }).exec();

            if (!book) return next(new Error('Book not found'));

            return res.status(200).send({success: true, book});
        } catch (error) {
            return res.json(error);
        }
    }
  },
  mutation: {
    createBooks: async (req, res, next) => {
          try {
              const { title, description } = req.body;
              
              const newBooks = new BooksSchema({title, description, user: req.user});

              const books = await newBooks.save();

              if (!books) throw new Error('Something went wrong');
              
              return res.status(200).send({success: true, books});
          } catch (error) {
            return res.json(error);
        }
    },
    updateBooks: async (req, res, next) => {
        try {
            const { title, description } = req.body;

            const { bookId } = req.params;

            // find bookId first
            const findBook = await BooksSchema.findById({ _id: bookId }).exec();

            if (!findBook) throw new Error('Book not exists');

            // check if user is authorized for this book 
            if (findBook.user.toString() == req.user._id.toString()) {
                console.log('here');
                // update book if authorized
                const updateBook = await BooksSchema.findByIdAndUpdate(
                    { _id: bookId }, { title, description }, { new: true }).exec();
                
                if (!updateBook) throw new Error('Something went wrong');
                
                return res.status(200).send({success: true, updateBook});
            } else {
                console.log('eles');
                throw new Error('You are not authorized to do changes in this book');
            }
        } catch (error) {
            return res.json(error);
        }
    },
    deleteBook: async (req, res, next) => {
        try {
            const { bookId } = req.params;

            // find bookId first
            const findBook = await BooksSchema.findById({ _id: bookId }).exec();

            if (!findBook) throw new Error('Book not exists');

            // check if user is authorized for this book 
            if (findBook.user.toString() == req.user._id.toString()) {

                // delete book if authorized
                const updateBook = await BooksSchema.findByIdAndRemove(
                    { _id: bookId }).exec();
                
                if (!updateBook) throw new Error('Something went wrong');
                
                return res.status(200).send({success: true, message: "Delete success!"});
            } else {
                throw new Error('You are not authorized to do changes in this book');
            }
        } catch (error) {
            return res.json(error);
        }
    }
  }
}


export default BooksController;
