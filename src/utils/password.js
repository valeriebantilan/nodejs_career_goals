import bcrypt from 'bcrypt';

export function comparePassword (candidatePassword, origPass, cb) {
    return bcrypt.compare(candidatePassword, origPass, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};