import jwt from 'jsonwebtoken';

export function verifyToken(token, ignoreExpiration = false) {
    return jwt.verify(token, process.env.JWT_SECRET, {
        ignoreExpiration,
    });
}

export function createToken(payload) {
    return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: process.env.JWT_EXPIRY });
}

export function createTokenWithExpiry(payload, exp) {
    const data = { ...payload };
    return jwt.sign(data, process.env.JWT_SECRET, {expiresIn: exp});
}

export function extractTokenFromRequest(request) {
    let authToken = null;

    const authorizationHeader = request.headers.authorization;

    if (authorizationHeader && authorizationHeader.split(' ')[0] === 'Bearer') {
        authToken = authorizationHeader.split(' ')[1];
    }

    return authToken;
}

export function createRefreshToken(payload) {
    return jwt.sign(payload, process.env.JWT_REFRESH_SECRET, { expiresIn: '7D' });
}
