import passport from 'passport';
import moment from 'moment';
import createError from 'http-errors';
import { verifyToken, extractTokenFromRequest } from '../utils/auth';

export const isAuthenticated = (req, res, next) => passport.authenticate('jwt', { session: false }, (err, user, info) => {
    if (info) {
        return next(new APIError(`Unauthorized_${info.name}: ${info.message}`, 401));
      }
    
  const token = extractTokenFromRequest(req);
  const verify = verifyToken(token);

  if (verify) {
    const { exp, iat } = verify;
        const expDate = new Date(exp*1000);
        const expiredDate = moment(expDate).format('YYYY-MM-DD hh:mm:ss');
        const date = moment().format('YYYY-MM-DD hh:mm:ss');
    
        if (new Date() > new Date(exp*1000)) return next(new APIError('Token expired', 401));
  }
    
  if (err) { return next(createError(403, err !== '' ? `Unauthorized: ${err}` : 'Unauthorized. You are not allowed to access this resource.')); } 

  if (!user) { return next(createError(403, 'Unauthorized. You are not allowed to access this resource.')) };

  req.user = user;
  next();
})(req, res, next);


