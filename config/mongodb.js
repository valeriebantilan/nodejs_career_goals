import mongoose from 'mongoose';
import { config } from './config';

const { MONGO_URI } = config;
 
mongoose.connect(MONGO_URI,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
    })
    .then(() => console.log('MongoDB connected'))
    .catch((e) => console.log('Error connection in database', e));