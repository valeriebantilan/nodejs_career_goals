module.exports = {
    allJS: ['src/server.js', 'config/*.js', 'src/services/**/*.js', 'test/**/*.js'],
    models: 'src/services/models/**.js',
    routes: 'src/services/routes/**.js',
    policies: 'src/services/**/*.policy.js'
}; 