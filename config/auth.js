import passport from 'passport';
import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt';
import UsersSchema from '../src/services/models/Users';

const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET,
};

passport.use(
    new JwtStrategy(opts, async(payload, done) => {
        console.log(payload);
        try {
            let result;


            result = await UsersSchema.findById({ _id: payload._id }).exec();

            console.log('result', result);

            if (result) {
                return done(null, result);
            } else {
                return done(null, null);
            }

        } catch (error) {
            return (error, null);
        }
    })
);
