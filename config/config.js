const assets = require('./assets/default');
const _ = require('lodash');
const glob = require('glob');
require('dotenv').config();
/**
 * Get files by glob patterns
 */
const getGlobbedPaths = function (globPatterns, excludes) {
  // URL paths regex
  const urlRegex = new RegExp('^(?:[a-z]+:)?\/\/', 'i');

  // The output array
  let output = [];


  // If glob pattern is array then we use each pattern in a recursive way, otherwise we use glob
  if (_.isArray(globPatterns)) {
    globPatterns.forEach(function (globPattern) {
      output = _.union(output, getGlobbedPaths(globPattern, excludes));
    });
  } else if (_.isString(globPatterns)) {
    if (urlRegex.test(globPatterns)) {
      output.push(globPatterns);
    } else {
      let files = glob.sync(globPatterns);
      if (excludes) {
        files = files.map(function (file) {
          if (_.isArray(excludes)) {
            for (let i in excludes) {
              file = file.replace(excludes[i], '');
            }
          } else {
            file = file.replace(excludes, '');
          }
          return file;
        });
      }
      output = _.union(output, files);
    }
  }

  return output;
};


const config = {
  files: {
    routes: getGlobbedPaths(assets.routes),
  },
  port: process.env.PORT,
  env: process.env.NODE_ENV,
  MONGO_URI: 'mongodb+srv://kan:J5yjc40dSLRKUrDg@nodejsmongodbcluster.80vvj.mongodb.net/nodejsmongodbtutodb?retryWrites=true&w=majority'
};
 
module.exports = {
  config,
}
