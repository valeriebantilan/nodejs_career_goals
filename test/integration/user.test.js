const chai = require('chai');
const httpStatus = require('http-status');
const mongoose = require('mongoose');
const request = require('supertest-as-promised');
const app = require('../../src/server');
const expect = chai.expect;

chai.config.includeStack = true;

describe('User Api', () => {
  let user = {
    firstName: 'Valerie',
    lastName: 'Bantilan',
    email: 'valerietest@gmail.com',
    password: 'testpassword'
  };

  const userId = '601a25b841665c2890a58dfcx';

  describe('# create post /api/user', () => {

    it('should create a new user', (done) => {
      request(app)
        .post('/api/user/create')
        .send(user)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.user.email).to.equal(user.email);
          user = res.body;
          done();
        })
        .catch(done);
    });

    it('should report error with message - Email already existed when checking email', (done) => {
      request(app)
        .post('/api/user/create')
        .expect(500)
        .send(user)
        .then((res) => {
          expect(res.body.message).to.equal('Internal Server Error');
          done();
        })
        .catch(done);
    });
  });

  describe('#Get /api/user/all', () => {
    it('should get all users', (done) => {
        request(app)
        .get(`/api/user/all`)
        .expect(httpStatus.OK)
          .then((res) => {
          expect(res.body.user).to.be.an('array')
          done();
        })
        .catch(done);
    })
  });
});

describe('User api endpoints with login', () => {
  let user = {};
  let userLogin = {}
  
  before((done) => {
    request(app)
    .post('/api/user/login')
    .send({
      email: 'valerietest@gmail.com',
      password: 'testpassword'
    })
    .end((err, response) => {
      userLogin = response.body; // save the token!
        done();
    });
  });

  describe('# update /api/user', () => {
    const updateUser = {
      firstName: "testVal"
    };

    it('should update user', (done) => {
      request(app)
        .post(`/api/user/update/${userLogin.user_id}`)
        .send(updateUser)
        .set('Authorization', `Bearer ${userLogin.accessToken}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.a('object');
          user = res.body;
          console.log('update', user);
          done();
        })
        .catch(done);
    });

    it('should report error with message - User not exists', (done) => {
      request(app)
        .post(`/api/user/update/${userLogin.user_id}`)
        .expect(500)
        .send(updateUser)
        .set('Authorization', userLogin.accessToken)
        .then((res) => {
          expect(res.body.message).to.equal('Internal Server Error');
          done();
        })
        .catch(done);
    });

  });
});