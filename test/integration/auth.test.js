const chai = require('chai');
const httpStatus = require('http-status');
const mongoose = require('mongoose');
const request = require('supertest-as-promised');
const app = require('../../src/server');
const expect = chai.expect;

chai.config.includeStack = true;

describe('Login Api', () => {
    let loginData = {
      email: 'valerietest@gmail.com',
      password: 'testpassword'
    };
    let user = {};

  describe('# login /api/user', () => {
    it('should login a user', (done) => {
      request(app)
        .post('/api/user/login')
        .send(loginData)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.accessToken).to.be.a('string');
            user = res.body;
          done();
        })
        .catch(done);
    });
      
    it('should report error with message - password mismatch', (done) => {
      request(app)
        .post('/api/user/login')
        .expect(500)
        .send(loginData)
        .then((res) => {
          expect(res.body.message).to.equal('Internal Server Error');
          done();
        })
        .catch(done);
    });
  });
});